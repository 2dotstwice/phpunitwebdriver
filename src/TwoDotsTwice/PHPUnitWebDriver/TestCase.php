<?php
/**
 * @file
 * Definition of TwoDotsTwice\PHPUnitWebDriver\TestCase
 */

namespace TwoDotsTwice\PHPUnitWebDriver;

use Imagine\Gd\Imagine;
use Imagine\Image\Box;
use Imagine\Image\Point;

/**
 * Class TestCase
 * @package TwoDotsTwice\PHPUnitWebDriver
 */
class TestCase extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \RemoteWebDriver
     */
    protected $driver;

    /**
     * @var string
     */
    protected $baseUrl;

    /**
     * @var string
     */
    protected $seleniumServerUrl;

    /**
     * @var string
     */
    protected $browserName;

    /**
     * @var string
     */
    protected $screenshotsDirectory;

    /**
     * @var integer
     */
    protected $screenshotsDirectoryMode;


    public function setUp()
    {
        $this->screenshotsDirectory = getenv('screenshots_directory') ? : 'test-results/screenshots';

        // @todo Test if the environment variable behaves well. It is a string, and actually we need an integer.
        $this->screenshotsDirectoryMode = getenv('screenshots_directory_mode') ? : 0700;

        $this->seleniumServerUrl = getenv('selenium_server_url') ? : 'http://localhost:4444/wd/hub';
        $this->browserName = getenv('browser_name') ? : 'firefox';

        // @todo Allow to pass more desired capabilities from configuration.
        $desiredCapabilities = array(
            \WebDriverCapabilityType::BROWSER_NAME => $this->browserName,
        );
        $this->driver = \RemoteWebDriver::create($this->seleniumServerUrl, $desiredCapabilities);

        $this->baseUrl = getenv('target_website_base_url');
    }

    public function tearDown()
    {
        $this->driver->quit();
    }

    /**
     * Construct a full URL.
     *
     * @param string $path The path relative to the base URL.
     *
     * @return string A full URL.
     */
    public function url($path = '')
    {
        $url = $this->baseUrl;

        if ($path) {
            $url .= '/' . $path;
        }

        return $url;
    }

    /**
     * Takes a screenshot and stores it on the file system.
     *
     * @param string $identifier A unique identifier, on which the file name of
     * the screenshot will be based.
     *
     * @param \WebDriverElement $element A webdriver element.
     *
     * @return NULL
     */
    public function takeScreenshot($identifier, \WebDriverElement $element = NULL)
    {
        if (!file_exists($this->screenshotsDirectory)) {
            mkdir($this->screenshotsDirectory, $this->screenshotsDirectoryMode, true);
        }
        $path = "{$this->screenshotsDirectory}/{$identifier}.png";
        $this->driver->takeScreenshot($path);

        // If a element was supplied, crop the image to the elements boundaries.
        if ($element) {
          $this->cropImageToElementBoundaries($element, $path);

        }
    }

  /**
   * @param \WebDriverElement $element
   * @param string $path
   */
  protected function cropImageToElementBoundaries(
    \WebDriverElement $element,
    $path
  ) {
    /** @var \WebDriverPoint $location */
    $location = $element->getLocation();
    $size = $element->getSize();

    $location->getX();
    $location->getY();
    $size->getHeight();
    $size->getWidth();

    $imagine = new Imagine();

    $img = $imagine->open($path);
    $img->crop(
      new Point($location->getX(), $location->getY()),
      new Box($size->getWidth(), $size->getHeight())
    );
    $img->save($path);
  }

}
